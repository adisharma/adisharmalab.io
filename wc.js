var dataTime = d3.range(0, 7).map(function(d) {
    return new Date(2014 + d, 10, 3);
  });

  var sliderTime = d3
    .sliderBottom()
    .min(d3.min(dataTime))
    .max(d3.max(dataTime))
    .step(1000 * 60 * 60 * 24 * 365)
    .width(300)
    .tickFormat(d3.timeFormat('%Y'))
    .tickValues(dataTime)
    .default(new Date(2014, 10, 3))
    .on('onchange', val => {
      var selected = d3.timeFormat('%Y')(val);
//Start cycling through the demo data
    showNewWords(myWordCloud,selected);
      //d3.select('p#value-time').text(d3.timeFormat('%Y')(val));
    });

  var gTime = d3
    .select('div#slider-time')
    .append('svg')
    .attr('width', 400)
    .attr('height', 100)
    .append('g')
    .attr('transform', 'translate(30,30)');
  
  function wordCloud(selector) {
    

    var fill = d3.scaleOrdinal(d3.schemeDark2);//d3.scale.category20();

    //Construct the word cloud's SVG element
    var svg = d3.select(selector).append("svg")
        .attr("width", 850)
        .attr("height", 200)
        .append("g")
        .attr("transform", "translate(425,100)");

    //Draw the word cloud
    function draw(words) {
        var cloud = svg.selectAll("g text")
                        .data(words, function(d) { return d.text; })
                        .style("margin-left", function(d) {return d.size + "px"})

        //Entering words
        cloud.enter()
            .append("text")
            .style("font-family", "Impact")
            .style("fill", function(d, i) { return fill(i); })
            .attr("text-anchor", "middle")
            .attr('font-size', 0)
            .text(function(d) { return d.text; });

        //Entering and existing words
        cloud
            .transition()
                .duration(600)
                .style("font-size", function(d) { return d.size + "px"; })
                .attr("transform", function(d) {
                    return "translate(" + [d.x, d.y] + ")rotate(" + d.rotate + ")";
                })
                .style("fill-opacity", 1);

        //Exiting words
        cloud.exit()
            .transition()
                .duration(200)
                .style('fill-opacity', 1e-6)
                .attr('font-size', 1)
                .remove();
    }


    //Use the module pattern to encapsulate the visualisation code. We'll
    // expose only the parts that need to be public.
    return {

        //Recompute the word cloud for a new set of words. This method will
        // asycnhronously call draw when the layout has been computed.
        //The outside world will need to call this function, so make it part
        // of the wordCloud return value.
        update: function(words) {
            d3.layout.cloud().size([850, 200])
                .words(words)
                .padding(1)
                .rotate(function() { return ~~(Math.random() * 2) * 90; })
                .font("Impact")
                //.spiral("rectangular")
                .spiral("archimedean")
                .fontSize(function(d) { return  d.size; })
                .on("end", draw)
                .start();
        }
    }

}

//Some sample data - http://en.wikiquote.org/wiki/Opening_lines
var words = [
"freshman year successfully completed high school getting good scores board exam joining prestigious instituion College Engineering Guindy CEG going studying Computer Science interests Computers always fascinated take studying Computer Science getting CEG cherry cake saw favourite football team, Arsenal win cup winning cup, Arsenal ended year draught without glory Arsenal favourite club love goes back long found couple people high school formed big gang friends CEG love friends even couple friends support Arsenal journey CEG started well playful kid worry grades struggled Engineerig Graphics first semester",
"family sophpmore year journey CEG roller coaster ride ups downs got first Arrear second semester Arrear comes core subject taken toll confidence decide take self projects something outside academia join Solarillion Foundation Research GPA reduced join Theatron second semester ended perfoming first play front thousand people love Theatron experience given great insight Theatre live form art Theatron Theatre given something look forward absolutely love friends start work Internet Things IoT take Motor Project using Arduino sucessfully finish Undergraduate Research Assistant Arsenal win another cup made year Arsenal best club world constant fear within regarding GPA GPA integral part academia friends gave amazing surprise 19th birthday treasure hunt family",
"family junior year GPA constantly decrease interested pursuing Masters USA worried GPA take break Theatron help Backstage Acting working Bluetooth Low Energy Internet Things fascinating wonderful change world give first attempt GRE turned horrible London Summer break visit Uncle, Aunt Cousins Emirates Stadium home Arsenal favourite Arsenal beautiful went birthday London much needed break academia finish research Bluetooth Low Energy take learning Machine Learning mini projects Machine Learning super exciting predicting future outcomes amazing interests Internet Things Machine Learning need increase GPA get top school USA friends family supportive respect academia try best increase GPA also excel Research publish papers top conferences",
"family senior year GPA taken hit rock bottom able concentrate academics research taken GRE gotten better score apply Universities USA wrote Statement Purpose research Internet Things accepted Conference Croatia presented work Croatia friends spent great time Croatia research Machine Learning regarding Flight Delay Prediction accepted Avionics Conference Florida USA presented work Florida won Best Paper Award Best Paper Award good look resume struggle Florida USA Visa put hold received 221g applied universities already started getting rejects couple lowest GPA sixth semester hurt chances get top school even great research wrapped research submitting Context Aware Bluetooth Low Energy paper top conference next months decide fate Masters USA Arsenal",
"applied many universities accepted Arizona State University ASU planning attend ASU Masters Computer Science Fall interested taking courses related Machine Learning Data Science research focus Machine Learning luck, got 221g student Visa reached Arizona started School ASU campus great new journey exciting first semester hectic tough Natural Language Processing Biomedical course interesting found research advisor Biodesign Institute currently work Statistician employment statistician exciting Healthcare Data Science caught attention believe next big thing visited New York two times New York city favourite city visited brother Boulder beautiful yet explore many places Arizona travel next year Arsenal still struggling family great, family love new journey staying away home exciting also scary get",
"second semester ASU great start securing Internship Amazon SDE Internship Amazon Connect summer Seattle going summer exciting internship great reality check research experience big jump professional career loved enjoyed work took time learn concepts technology wasn't hoped interacted worked Product Managers Software Development Managers opportunity opened thinking explore learn Product Management great career choice sounds exciting would love pivot away development second year Masters returning Amazon two faced Great experience job hunt looking opportunities pivot away Coding received offer MathWorks decided take perfect opportunity interest going home Chennai India December winter break eager visit family Friends back home Chennai Arsenal Seattle amazing",
"2020 bad start enjoyed time Chennai India family friends secured job MathWorks intended travel lot semester Everything got spoilt beacuse Corona Virus outbreak COVID19 disrupted world everyone lockdown Stay home orders effective USA Arizona graduated ASU online ceremony plan start work Application Engineer MathWorks July mostly work home COVID19 made study Stock Market invest Stock Market pretty interesting started investing small amounts still student interest Stock peaked invest future think MathWorks perfect opportunity pivot Product Management Development Corona Virus COVID19 testing time hopefully comes end soon suffocating boring lockdown Houseparty Zoom calls friends thing challenging times Arsenal still heart blood miss soccer sporting events canceled Zoom"
]

//Prepare one of the sample sentences by removing punctuation,
// creating an array of words and computing a random size attribute.
var stopwords = ['i','me','my','myself','we','our','ours','ourselves','you','your','yours','yourself','yourselves','he','him','his','himself','she','her','hers','herself','it','its','itself','they','them','their','theirs','themselves','what','which','who','whom','this','that','these','those','am','is','are','was','were','be','been','being','have','has','had','having','do','does','did','doing','a','an','the','and','but','if','or','because','as','until','while','of','at','by','for','with','about','against','between','into','through','during','before','after','above','below','to','from','up','down','in','out','on','off','over','under','again','further','then','once','here','there','when','where','why','how','all','any','both','each','few','more','most','other','some','such','no','nor','not','only','own','same','so','than','too','very','s','t','can','will','just','don','should','now']
function removeStopWords(str) {
    res = []
    w = str.split(' ')
    for(i=0;i<w.length;i++) {
       word_clean = w[i].split(".").join("")
       if(!stopwords.includes(word_clean.toLowerCase()) && (word_clean.length > 2)) {
           res.push(word_clean)
       }
    }
    console.log(res.join(' '));
    return(res.join(' '))
}
function getWords(i, words) {
    var word_count = {};
            var ind = i - 2014;
            //var temp = removeStopWords(words[i - '2014']);
            var temp = words[ind];
            var str =  temp.split(/[ '\-\(\)\*":;\[\]|{},.!?]+/);
            var len = str.len
            str.forEach(function(word){
                if (word_count[word]){
                  word_count[word]++;
                } else {
                  word_count[word] = 1;
                }
              })
    return words[ind]
            .replace(/[!\.,:;\?]/g, '')
            .split(' ')
            .map(function(d) {
                
                return {text: d, size: 5 + 10*word_count[d]};
            })} 

//This method tells the word cloud to redraw with a new set of words.
//In reality the new words would probably come from a server request,
// user input or some other source.
function showNewWords(vis, year) {
    //i = i || 0;
    
    vis.update(getWords(year, words))
    
    //setTimeout(function() { showNewWords(vis, i + 1)}, 2000)
}

//Create a new instance of the word cloud visualisation.
var myWordCloud = wordCloud('#sli');
gTime.call(sliderTime);
showNewWords(myWordCloud, "2014");
showNewWords(myWordCloud, d3.timeFormat('%Y')(sliderTime.value()));
